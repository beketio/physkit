package projects.physkit.tools;

public class MathTools {

	public static double dist(double x, double y) {
		return Math.sqrt(x * x + y * y);
	}

}
