package projects.physkit.tools;

import projects.physkit.model.EFObjects.Charge;

public class CoulombsToolkit {

	public static final double COULOMB_K = 8.9875517873681764e9;
	public static final double ELECTRON_CHARGE = 1.60217657e-19;
	public static final double ELECTRON_WIEGHT = 0;

	public static PhysVector getForceVector(Charge c1, Charge c2) {
		PhysVector vec = new PhysVector(c2.getXPos() - c1.getXPos(),
				c2.getYPos() - c1.getYPos(), 0);
		double mag = (COULOMB_K * c1.getCharge() * c2.getCharge())
				/ Math.pow(vec.getMagnitude().eval(), 2);
		vec.makeUnitVector();
		vec.multiply(-mag);
		return vec;
	}

	public static PhysVector getFieldVector(double xPos, double yPos,
			Charge charge) {
		PhysVector vec = new PhysVector(charge.getXPos() - xPos,
				charge.getYPos() - yPos, 0);
		double mag = (COULOMB_K * charge.getCharge());
		mag /= Math.pow(vec.getMagnitude().eval(xPos, yPos, 0), 2);
		vec.makeUnitVector();
		vec.multiply(mag);
		vec.multiply(-1);
		// System.out.println(vec);
		return vec;
	}
}
