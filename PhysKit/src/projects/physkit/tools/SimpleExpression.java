package projects.physkit.tools;

import uk.co.cogitolearning.cogpar.EvaluationException;
import uk.co.cogitolearning.cogpar.ExpressionNode;
import uk.co.cogitolearning.cogpar.Parser;
import uk.co.cogitolearning.cogpar.ParserException;

public class SimpleExpression extends ExpressionWrapper {

	double var = 0;

	public SimpleExpression() {
	}

	public SimpleExpression(String str) {
		parse(str);
	}

	public SimpleExpression(double val) {
		var = val;
	}

	private void parse(String str) {
		Parser parser = new Parser();
		try {
			ExpressionNode expr = parser.parse(str);
			var = expr.getValue();
		} catch (ParserException e) {
			System.out.println(e.getMessage());
		} catch (EvaluationException e) {
			System.out.println(e.getMessage());
		}
	}

	public void add(Object add) {
		var += getVal(add);
	}

	public void sub(Object sub) {
		var -= getVal(sub);
	}

	public void mult(Object mult) {
		var *= getVal(mult);
	}

	public void div(Object div) {
		var /= getVal(div);
	}

	public void exp(Object exp) {
		var = Math.pow(var, getVal(exp));
	}

	public void sqrt() {
		var = Math.sqrt(var);
	}
	
	public void add(ExpressionWrapper add) {
		var += getVal(add);
	}

	public void sub(ExpressionWrapper sub) {
		var -= getVal(sub);
	}

	public void mult(ExpressionWrapper mult) {
		var *= getVal(mult);
	}

	public void div(ExpressionWrapper div) {
		var /= getVal(div);
	}

	public void exp(ExpressionWrapper exp) {
		var = Math.pow(var, getVal(exp));
	}

	public double eval(double x, double y, double z) {
		return var;
	}

	public double eval(double x, double y) {
		return eval(x, y, 0);
	}

	public double eval() {
		return eval(0, 0, 0);
	}

	public String toString() {
		return var + "";
	}

	private double getVal(Object ob) {
		if (ob instanceof SimpleExpression)
			return ((SimpleExpression) ob).eval();
		if (ob instanceof String)
			return new SimpleExpression((String) ob).eval();
		return (double) ob;
	}

	@Override
	public ExpressionWrapper clone() {
		return new SimpleExpression(var);
	}
}
