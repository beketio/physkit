package projects.physkit.tools;

public class oldPhysVector {

	double i, j, k;

	public oldPhysVector() {
		i = 0;
		j = 0;
		k = 0;
	}

	public oldPhysVector(double i, double j, double k) {
		this.i = i;
		this.j = j;
		this.k = k;
	}

	public double getMagnitude() {
		return Math.sqrt(i * i + j * j + k * k);
	}

	public double getI() {
		return i;
	}

	public double getJ() {
		return j;
	}

	public double getK() {
		return k;
	}

	public void makeUnitVector() {
		double mag = getMagnitude();
		if (mag > 0) {
			i /= mag;
			j /= mag;
			k /= mag;
		}
	}

	public oldPhysVector clone() {
		return new oldPhysVector(i, j, k);
	}

	public void normalizeMagnitude() {
		double newMag = Math.log(this.getMagnitude() + 1);
		this.makeUnitVector();
		this.multiply(newMag);
	}

	public void add(oldPhysVector vec) {
		i += vec.getI();
		j += vec.getJ();
		k += vec.getK();
	}

	public void multiply(double num) {
		i *= num;
		j *= num;
		k *= num;
	}

	@Override
	public String toString() {
		return i + "i + " + j + "j + " + k + "k";
	}

}
