package projects.physkit.tools;

import uk.co.cogitolearning.cogpar.EvaluationException;
import uk.co.cogitolearning.cogpar.ExpressionNode;
import uk.co.cogitolearning.cogpar.Parser;
import uk.co.cogitolearning.cogpar.ParserException;
import uk.co.cogitolearning.cogpar.SetVariable;

public class VariableExpression extends ExpressionWrapper {

	String exprstr = "";

	public VariableExpression() {
		exprstr = "0";
	}

	public VariableExpression(String str) {
		exprstr = str;
	}

	public VariableExpression(double val) {
		this(Double.toString(val));
	}

	public void add(Object add) {
		if (compatible(add))
			exprstr = "(" + exprstr + ")+(" + add + ")";
	}

	public void sub(Object sub) {
		if (compatible(sub))
			exprstr = "(" + exprstr + ")-(" + sub + ")";
	}

	public void mult(Object mult) {
		if (compatible(mult))
			exprstr = "(" + exprstr + ")*(" + mult + ")";
	}

	public void div(Object div) {
		if (compatible(div))
			exprstr = "(" + exprstr + ")/(" + div + ")";
	}

	public void exp(Object exp) {
		if (compatible(exp))
			exprstr = "(" + exprstr + ")^(" + exp + ")";
	}

	public void sqrt() {
		exprstr = "sqrt(" + exprstr + ")";
	}

	public double eval(double x, double y, double z) {
		// exprstr = exprstr.replaceAll("\\+{2}", "+");
		// exprstr = exprstr.replaceAll("\\+-", "-");
		Parser parser = new Parser();
		try {
			ExpressionNode expr = parser.parse(exprstr);
			expr.accept(new SetVariable("x", x));
			expr.accept(new SetVariable("y", y));
			expr.accept(new SetVariable("z", z));
			return expr.getValue();
		} catch (ParserException e) {
			System.out.println(e.getMessage());
		} catch (EvaluationException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}

	public double eval(double x, double y) {
		return eval(x, y, 0);
	}

	public double eval() {
		return eval(0, 0, 0);
	}

	public String toString() {
		return exprstr;
	}

	private static boolean compatible(Object obj) {
		return obj instanceof ExpressionWrapper || obj instanceof String
				|| Double.class.isInstance(obj)
				|| Integer.class.isInstance(obj);
	}
}
