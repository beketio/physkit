package projects.physkit.tools;

public class PhysVector {

	ExpressionWrapper i, j, k;

	public PhysVector() {
		i = ExpressionWrapper.make();
		j = ExpressionWrapper.make();
		k = ExpressionWrapper.make();
	}

	public PhysVector(double i, double j, double k) {
		this.i = ExpressionWrapper.make(i);
		this.j = ExpressionWrapper.make(j);
		this.k = ExpressionWrapper.make(k);
	}

	public PhysVector(ExpressionWrapper i, ExpressionWrapper j, ExpressionWrapper k) {
		this.i = i;
		this.j = j;
		this.k = k;
	}

	public ExpressionWrapper getMagnitude() {
		return ExpressionWrapper.sqrt(ExpressionWrapper.exp(i, 2) + "+"
				+ ExpressionWrapper.exp(j, 2) + "+" + ExpressionWrapper.exp(k, 2));
	}

	public double getMagnitude(double x, double y) {
		return Math.sqrt(ExpressionWrapper.exp(i, 2).eval(x, y)
				+ ExpressionWrapper.exp(j, 2).eval(x, y)
				+ ExpressionWrapper.exp(k, 2).eval(x, y));
	}

	public ExpressionWrapper getI() {
		return i;
	}

	public ExpressionWrapper getJ() {
		return j;
	}

	public ExpressionWrapper getK() {
		return k;
	}

	public void makeUnitVector() {
		ExpressionWrapper mag = getMagnitude();
		i = ExpressionWrapper.div(i, mag);
		j = ExpressionWrapper.div(j, mag);
		k = ExpressionWrapper.div(k, mag);
	}

	public PhysVector clone() {
		return new PhysVector(i.clone(), j.clone(), k.clone());
	}

	public void normalizeMagnitude(double x, double y) {
		double newMag = Math.log(this.getMagnitude().eval(x, y, 0) + 1) * 20;
		this.makeUnitVector();
		this.multiply(newMag);
	}

	public void add(PhysVector vec) {
		i = ExpressionWrapper.add(i, vec.getI());
		j = ExpressionWrapper.add(j, vec.getJ());
		k = ExpressionWrapper.add(k, vec.getK());
	}

	public void multiply(double num) {
		i = ExpressionWrapper.mult(i, num);
		j = ExpressionWrapper.mult(j, num);
		k = ExpressionWrapper.mult(k, num);
	}

	@Override
	public String toString() {
		return i + "i + " + j + "j + " + k + "k";
	}

}
