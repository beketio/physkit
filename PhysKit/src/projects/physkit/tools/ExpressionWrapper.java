package projects.physkit.tools;

public abstract class ExpressionWrapper {

	public abstract void add(Object add);

	public abstract void sub(Object sub);

	public abstract void mult(Object mult);

	public abstract void div(Object div);

	public abstract void exp(Object exp);

	public abstract void sqrt();

	public abstract double eval(double x, double y, double z);

	public abstract double eval(double x, double y);

	public abstract double eval();

	public abstract String toString();

	public ExpressionWrapper clone() {
		return make(toString());
	}

	private static boolean compatible(Object obj) {
		return true;
		// return obj instanceof Expression || obj instanceof String
		// || Double.class.isInstance(obj)
		// || Integer.class.isInstance(obj);
	}

	/* STATIC METHODS */

	public static ExpressionWrapper make(Object s) {
		if (s instanceof String)
			if (((String) s).matches("(.*)[xyz](.*)")) {
				return new VariableExpression((String) s);
			} else
				return new SimpleExpression((String) s);
		return new SimpleExpression((double) s);
	}

	public static ExpressionWrapper make() {
		return new SimpleExpression(0);
	}

	public static ExpressionWrapper add(Object add1, Object add2) {
		if (compatible(add1) && compatible(add2))
			return make("(" + add1 + ")+(" + add2 + ")");
		return null;
	}

	public static ExpressionWrapper sub(Object sub1, Object sub2) {
		if (compatible(sub1) && compatible(sub2))
			return make("(" + sub1 + ")-(" + sub2 + ")");
		return null;
	}

	public static ExpressionWrapper mult(Object mult1, Object mult2) {
		if (compatible(mult1) && compatible(mult2))
			return make("(" + mult1 + ")*(" + mult2 + ")");
		return null;
	}

	public static ExpressionWrapper div(Object div1, Object div2) {
		if (compatible(div1) && compatible(div2))
			return make("(" + div1 + ")/(" + div2 + ")");
		return null;
	}

	public static ExpressionWrapper exp(Object exp1, Object exp2) {
		if (compatible(exp1) && compatible(exp2))
			return make("(" + exp1 + ")^(" + exp2 + ")");
		return null;
	}

	public static ExpressionWrapper sqrt(Object sqrt) {
		if (compatible(sqrt))
			return make("sqrt(" + sqrt + ")");
		return null;
	}
}
