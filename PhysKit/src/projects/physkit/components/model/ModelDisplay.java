package projects.physkit.components.model;

import java.awt.GridLayout;

import projects.physkit.components.AbstractPhysComponent;
import projects.physkit.components.model.mdis.ChargeDisplayItem;
import projects.physkit.globalvars.ObRefs;
import projects.physkit.model.EField;
import projects.physkit.model.FieldObject;

public class ModelDisplay extends AbstractPhysComponent {

	private EField field = ObRefs.eField;

	public ModelDisplay() {
		this.setLayout(new GridLayout(field.getEFObjects().size(), 1));
		// populate();
	}

	private void populate() {
		for (FieldObject ob : field.getEFObjects()) {
			this.add(new ChargeDisplayItem(ob));
		}
	}

}
