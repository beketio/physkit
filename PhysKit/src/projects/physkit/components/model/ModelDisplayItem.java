package projects.physkit.components.model;

import java.awt.GridLayout;

import javax.swing.JComponent;

import projects.physkit.model.FieldObject;

public abstract class ModelDisplayItem extends JComponent {

	protected GridLayout components;

	public ModelDisplayItem(int cols) {
		components = new GridLayout(1, cols);
		this.setLayout(components);
	}

}
