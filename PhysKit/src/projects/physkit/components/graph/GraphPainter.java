package projects.physkit.components.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.List;

import projects.physkit.globalvars.GraphConfig;
import projects.physkit.globalvars.ObRefs;
import projects.physkit.model.FieldObject;
import projects.physkit.model.EFObjects.Charge;
import projects.physkit.model.EFObjects.FieldCalculator;
import projects.physkit.tools.PhysVector;

public class GraphPainter {

	public static int scaleX(double num) {
		return (int) (num * GraphConfig.xScale + 0.5);
	}

	public static int scaleY(double num) {
		return (int) -(num * GraphConfig.yScale + 0.5);
	}

	public static void paintAxis(int width, int height, Graphics2D g) {
		g.setStroke(new BasicStroke(2));
		g.drawLine(0, (int) (-height / 2 + scaleY(GraphConfig.yPos)), 0,
				(int) (height / 2 + scaleY(GraphConfig.yPos)));
		g.drawLine((int) (-width / 2 + scaleX(GraphConfig.xPos)), 0,
				(int) (width / 2 + scaleX(GraphConfig.xPos)), 0);
		g.setStroke(new BasicStroke());
	}

	public static void paintGrid(int width, int height, Graphics2D g) {
		int xLines = (int) (((width / 2) / GraphConfig.xScale) / GraphConfig.xStep) + 1;
		int yLines = (int) (((height / 2) / GraphConfig.yScale) / GraphConfig.yStep) + 1;

		g.setColor(Color.LIGHT_GRAY);
		AffineTransform trans = g.getTransform();
		g.translate(scaleX(GraphConfig.xPos), 0);
		for (int i = -xLines; i <= xLines; i++) {
			int xLine = (int) (-((GraphConfig.xPos * GraphConfig.xScale) % (GraphConfig.xStep * GraphConfig.xScale)) + GraphConfig.xStep
					* GraphConfig.xScale * i);
			g.drawLine(xLine, (int) (-height / 2 + scaleY(GraphConfig.yPos)),
					xLine, (int) (height / 2 + scaleY(GraphConfig.yPos)));
		}
		g.setTransform(trans);
		g.translate(0, scaleY(GraphConfig.yPos));
		for (int i = -yLines; i <= yLines; i++) {
			int yLine = (int) (((GraphConfig.yPos * GraphConfig.yScale) % (GraphConfig.yStep * GraphConfig.yScale)) + GraphConfig.yStep
					* GraphConfig.yScale * i);
			g.drawLine((int) (-width / 2 + scaleX(GraphConfig.xPos)), yLine,
					(int) (width / 2 + scaleX(GraphConfig.xPos)), yLine);
		}
		// g.translate(scaleX(GraphConfig.xPos), 0);
		// for (int x = -xLines; x <= xLines; x++) {
		// for (int y = -yLines; y <= yLines; y++) {
		// int xLine = (int) (-((GraphConfig.xPos * GraphConfig.xScale) %
		// (GraphConfig.xStep * GraphConfig.xScale)) + GraphConfig.xStep
		// * GraphConfig.xScale * x);
		// int yLine = (int) (((GraphConfig.yPos * GraphConfig.yScale) %
		// (GraphConfig.yStep * GraphConfig.yScale)) + GraphConfig.yStep
		// * GraphConfig.yScale * y);
		// FieldCalculator fc = new FieldCalculator(GraphConfig.xPos,
		// GraphConfig.yPos);
		// fc.update(ObRefs.eField.getEFObjects());
		// PhysVector vec = fc.getFieldVector();
		// vec.makeUnitVector();
		// vec.multiply(GraphConfig.xStep * GraphConfig.xScale);
		// paintVector(xLine, -yLine, vec, g);
		// }
		// }
		g.setTransform(trans);

		g.setColor(Color.BLACK);

		// double xmin = GraphConfig.xPos - (width / 2) / GraphConfig.xScale;
		// double xmax = GraphConfig.xPos + (width / 2) / GraphConfig.xScale;
		// double ymin = GraphConfig.yPos - (height / 2) / GraphConfig.xScale;
		// double ymax = GraphConfig.yPos + (height / 2) / GraphConfig.xScale;
		//
		// int xLines = (int) ((xmax - xmin) / GraphConfig.xStep);
		// int yLines = (int) ((ymax - ymin) / GraphConfig.yStep);

	}

	public static void paintCharge(Charge c, Graphics g) {
		if (c.getCharge() > 0)
			g.setColor(Color.RED);
		else
			g.setColor(Color.BLUE);
		g.fillOval(scaleX(c.getXPos()) - 5, scaleY(c.getYPos()) - 5, 10, 10);
	}

	public static void paintFieldCalculator(FieldCalculator fc, Graphics g) {
		g.setColor(Color.GREEN);
		g.fillOval(scaleX(fc.getXPos()) - 5, scaleY(fc.getYPos()) - 5, 10, 10);
	}

	public static void paintVector(double xPos, double yPos, PhysVector v,
			Graphics g) {
		g.setColor(Color.RED);
		PhysVector vec = v.clone();
		vec.normalizeMagnitude(xPos, yPos);
		// vec.multiply(1);
		int xEnd = scaleX(xPos + vec.getI().eval(xPos, yPos));
		int yEnd = scaleY(yPos + vec.getJ().eval(xPos, yPos));
		g.drawLine(scaleX(xPos), scaleY(yPos), xEnd, yEnd);
		double mag = 8;
		double theta = Math.atan2(v.getJ().eval(xPos, yPos),
				-v.getI().eval(xPos, yPos));
		g.drawLine(xEnd, yEnd,
				xEnd + (int) (mag * Math.cos(theta + Math.PI / 4) + 0.5), yEnd
						+ (int) (mag * Math.sin(theta + Math.PI / 4) + 0.5));
		g.drawLine(xEnd, yEnd,
				xEnd + (int) (mag * Math.cos(theta - Math.PI / 4) + 0.5), yEnd
						+ (int) (mag * Math.sin(theta - Math.PI / 4) + 0.5));
	}

	public static void paintStrings(double xPos, double yPos, String[] lines,
			Graphics g) {
		g.setColor(Color.BLACK);
		FontMetrics fm = ((Graphics2D) g).getFontMetrics();
		for (int i = 0; i < lines.length; i++)
			g.drawString(lines[i], scaleX(xPos) - fm.stringWidth(lines[i]) / 2,
					scaleY(yPos) - (fm.getHeight() - fm.getDescent()) * i - 5);

	}

	public static String[] getStrings(FieldObject ob) {
		List<String> strs = new ArrayList<String>();
		if (ob instanceof Charge) {
			if (GraphConfig.paintChargeMagnitude)
				strs.add(((Charge) ob).getForceMagnitude() + "N");
			if (GraphConfig.paintChargePos)
				strs.add("(" + ob.getXPos() + ", " + ob.getYPos() + ")");
			if (GraphConfig.paintChargeValue)
				strs.add(((Charge) ob).getCharge() + "C");
			if (GraphConfig.paintChargeName)
				strs.add(ob.getName());
		} else if (ob instanceof FieldCalculator) {
			if (GraphConfig.paintFCMagnitude)
				strs.add(((FieldCalculator) ob).getFieldMagnitude() + "N/C");
			if (GraphConfig.paintFCPos)
				strs.add("(" + ob.getXPos() + ", " + ob.getYPos() + ")");
			if (GraphConfig.paintChargeName)
				strs.add(ob.getName());
		}
		return strs.toArray(new String[strs.size()]);
	}

	private GraphPainter() {

	}

}
