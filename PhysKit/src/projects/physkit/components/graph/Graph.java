package projects.physkit.components.graph;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import projects.physkit.globalvars.GraphConfig;
import projects.physkit.globalvars.ObRefs;
import projects.physkit.model.EField;
import projects.physkit.model.FieldObject;
import projects.physkit.model.EFObjects.Charge;
import projects.physkit.model.EFObjects.FieldCalculator;

public class Graph {

	private EField field;

	private int width = 0, height = 0;

	public Graph() {
		field = ObRefs.eField;
		// field.add(new Charge("test", 60, 100, 5.6e-2));
		// field.add(new Charge("test", 70, 500, -7.8e-2));
		// field.add(new FieldCalculator("", -80, 200));
		// field.add(new FieldCalculator("", 90, 300));
		// field.add(new FieldCalculator("", 200, -200));
		// field.add(new FieldCalculator("", 20, 100));
		// field.add(new FieldCalculator("", -60, 50));
		// field.add(new FieldCalculator("", 400, 500));

		// field.add(new Charge("Q1", -0.7, 0, 4.6e-6));
		// field.add(new Charge("Q2", 0.7, 0, -4.6e-6));
		for (int i = 0; i < 10; i++)
			field.add(new Charge("", Math.random() * 200 - 100,
					Math.random() * 200 - 100, (Math.random() - 0.5) / 10000));
		for (int i = 0; i < 10; i++)
			field.add(new FieldCalculator("", Math.random() * 200 - 100, Math
					.random() * 200 - 100));
		field.add(new Charge("Q3", 0, -0.207, -4.5e-9));
		field.add(new Charge("Q4", 1, 1.011, -4.9e-6));
		field.add(new FieldCalculator(-100, 0));
		field.add(new FieldCalculator(0, 100));
		// field.add(new UniformField("5*x^2", "6"));
		// field.add(field.getBalancePoint());
	}

	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public void draw(Graphics2D g) {
		g.translate(width / 2 - GraphConfig.xPos * GraphConfig.xScale, height
				/ 2 + GraphConfig.yPos * GraphConfig.yScale);
		paintGrid(g);
		paintAxis(g);
		drawVectors(g);
		drawObjects(g);
		drawStrings(g);
		g.setTransform(new AffineTransform());
		g.setColor(Color.BLACK);
		FontMetrics fm = g.getFontMetrics();
		String pos = "(" + GraphConfig.xPos + ", " + GraphConfig.yPos + ")";
		g.drawString(pos, 0, fm.getHeight() - 2);
		String step = "";
		if (GraphConfig.xStep == GraphConfig.yStep)
			step = "Step: " + GraphConfig.xStep + "m";
		else
			step = "X Step: " + GraphConfig.xStep + "m, Y Step: "
					+ GraphConfig.yStep + "m";
		g.drawString(step, width - fm.stringWidth(step) - 4, fm.getHeight() - 2);
		String scale = "";
		if (GraphConfig.xScale == GraphConfig.yScale)
			scale = "Zoom: x" + GraphConfig.xScale;
		else
			scale = "X Zoom: x" + GraphConfig.xScale + ", Y Zoom: x"
					+ GraphConfig.yScale;
		g.drawString(scale, width - fm.stringWidth(scale) - 4,
				(fm.getHeight() - 2) * 2);
	}

	private void drawVectors(Graphics g) {
		for (FieldObject ob : field.getEFObjects())
			if (ob instanceof Charge && GraphConfig.paintChargeVector)
				GraphPainter.paintVector(ob.getXPos(), ob.getYPos(),
						((Charge) ob).getForceVector(), g);
			else if (ob instanceof FieldCalculator && GraphConfig.paintFCVector)
				GraphPainter.paintVector(ob.getXPos(), ob.getYPos(),
						((FieldCalculator) ob).getFieldVector(), g);
	}

	private void drawObjects(Graphics g) {
		for (FieldObject ob : field.getEFObjects())
			if (ob instanceof Charge)
				GraphPainter.paintCharge((Charge) ob, g);
			else if (ob instanceof FieldCalculator)
				GraphPainter.paintFieldCalculator((FieldCalculator) ob, g);
	}

	private void drawStrings(Graphics g) {
		for (FieldObject ob : field.getEFObjects())
			GraphPainter.paintStrings(ob.getXPos(), ob.getYPos(),
					GraphPainter.getStrings(ob), g);

	}

	private void paintAxis(Graphics2D g) {
		GraphPainter.paintAxis(width, height, g);
	}

	private void paintGrid(Graphics2D g) {
		g.setColor(Color.BLACK);
		GraphPainter.paintGrid(width, height, g);
		// g.drawLine(width / 2, 0, width / 2, height);
		// g.drawLine(0, height / 2, width, height / 2);
	}

}
