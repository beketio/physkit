package projects.physkit.components.graph;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import projects.physkit.components.AbstractPhysComponent;
import projects.physkit.globalvars.GraphConfig;

public class GraphDisplay extends AbstractPhysComponent {

	private Graph graph;

	private int dragX = 0, dragY = 0;
	private boolean drag = false;

	public GraphDisplay() {
		this.addComponentListener(this);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
		graph = new Graph();
	}

	@Override
	public void componentResized(ComponentEvent e) {
		graph.setSize(this.getWidth(), this.getHeight());
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		graph.setSize(this.getWidth(), this.getHeight());
		if (graph != null)
			graph.draw((Graphics2D) g);
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		if (evt.getButton() == 1) {
			drag = true;
			dragX = evt.getX();
			dragY = evt.getY();
		}
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		if (evt.getButton() == 1)
			drag = false;
	}

	@Override
	public void mouseDragged(MouseEvent evt) {
		if (!drag)
			return;
		int dx = dragX - evt.getX();
		int dy = dragY - evt.getY();
		GraphConfig.xPos += dx / GraphConfig.xScale;
		GraphConfig.yPos += -dy / GraphConfig.yScale;
		dragX = evt.getX();
		dragY = evt.getY();
		repaint();
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent evt) {
		int dmm = -evt.getWheelRotation();
		double zFactor = 0.1;
		GraphConfig.xPos -= ((evt.getX() - this.getWidth() / 2)
				/ (1 + zFactor * dmm) - (evt.getX() - this.getWidth() / 2))
				/ GraphConfig.xScale;
		GraphConfig.yPos += ((evt.getY() - this.getHeight() / 2)
				/ (1 + zFactor * dmm) - (evt.getY() - this.getHeight() / 2))
				/ GraphConfig.yScale;

		GraphConfig.xScale *= (1 + zFactor * dmm);
		GraphConfig.yScale *= (1 + zFactor * dmm);
		if (GraphConfig.xScale * GraphConfig.xStep > this.getWidth() / 4) {
			GraphConfig.xStep /= 10;
			GraphConfig.yStep /= 10;
		} else if (GraphConfig.xScale * GraphConfig.xStep < this.getWidth() / 100) {
			GraphConfig.xStep *= 10;
			GraphConfig.yStep *= 10;
		}
		repaint();
	}

}
