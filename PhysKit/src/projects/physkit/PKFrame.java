package projects.physkit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;

import projects.physkit.components.graph.GraphDisplay;
import projects.physkit.globalvars.FrameConfig;
import projects.physkit.globalvars.GraphConfig;
import projects.physkit.globalvars.MenuConfig;

public class PKFrame extends JFrame {

	private GraphDisplay graphDisplay;

	/**
	 * 
	 */
	private static final long serialVersionUID = -583115879034440687L;

	public PKFrame() {
		initFrame();
		initComponents();
		this.setVisible(true);
		System.out.println("donneee");
		this.repaint();
	}

	private void initFrame() {
		this.setSize(FrameConfig.DEFAULT_FRAME_WIDTH,
				FrameConfig.DEFAULT_FRAME_HEIGHT);
		this.setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	private void initComponents() {
		JTabbedPane tabPane = new JTabbedPane();

		initMenu();
		graphDisplay = new GraphDisplay();
		tabPane.add("Graph", graphDisplay);

		this.add(tabPane);
	}

	private void initMenu() {
		PKMenuListener mListener = new PKMenuListener();

		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(fileMenu);
		JMenu addMenu = new JMenu("Add");
		addMenu.setMnemonic(KeyEvent.VK_A);
		menuBar.add(addMenu);
		JMenu removeMenu = new JMenu("Remove");
		removeMenu.setMnemonic(KeyEvent.VK_R);
		menuBar.add(removeMenu);
		JMenu configMenu = new JMenu("Config");
		configMenu.setMnemonic(KeyEvent.VK_C);
		menuBar.add(configMenu);

		JMenu graphMenu = new JMenu("Graph");
		graphMenu.setMnemonic(KeyEvent.VK_G);
		menuBar.add(graphMenu);

		JMenuItem zStandard = new JMenuItem("Zoom Standard");
		graphMenu.add(zStandard);
		JMenuItem zFit = new JMenuItem("Zoom Fit");
		graphMenu.add(zFit);
		graphMenu.addSeparator();
		Object[] config = MenuConfig.config;
		for (int i = 0; i < config.length - 1; i += 2) {
			JCheckBoxMenuItem cbmi = new JCheckBoxMenuItem((String) config[i],
					(Boolean) config[i + 1]);
			cbmi.addItemListener(mListener);
			graphMenu.add(cbmi);
		}

		this.setJMenuBar(menuBar);
	}

	private class PKMenuListener implements ActionListener, ItemListener {

		private Object[] config = MenuConfig.config;

		@Override
		public void itemStateChanged(ItemEvent evt) {

			
			JCheckBoxMenuItem source = (JCheckBoxMenuItem) evt.getSource();


			for (int i = 0; i < config.length; i++) {
				if (source.getText().equals(config[i])) {
					config[i + 1] = !((Boolean) config[i + 1]);
				}
			}
			repaint();

		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

		}

	}

}
