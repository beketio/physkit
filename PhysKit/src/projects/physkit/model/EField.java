package projects.physkit.model;

import java.util.ArrayList;
import java.util.List;

import projects.physkit.model.EFObjects.Charge;
import projects.physkit.model.EFObjects.FieldCalculator;
import projects.physkit.tools.PhysVector;

public class EField {

	private List<FieldObject> efObjects = new ArrayList<FieldObject>();

	public List<FieldObject> getEFObjects() {
		return efObjects;
	}

	public void add(FieldObject ob) {
		efObjects.add(ob);
		update();
	}

	public void remove(FieldObject ob) {
		efObjects.remove(ob);
		update();
	}

	public void update() {
		for (FieldObject ob : efObjects)
			ob.update(efObjects);
	}

	public FieldCalculator getBalancePoint() {
		Charge c1 = null;
		Charge c2 = null;
		for (FieldObject ob : efObjects)
			if (c1 == null && ob instanceof Charge)
				c1 = (Charge) ob;
			else if (c2 == null && ob instanceof Charge)
				c2 = (Charge) ob;
			else if (ob instanceof Charge)
				return null;
		if ((c1 == null || c2 == null)
				|| (c1.getCharge() > 0 ^ c2.getCharge() > 0))
			return null;
		PhysVector diffV = new PhysVector(c2.getXPos() - c1.getXPos(),
				c2.getYPos() - c1.getYPos(), 0);
		System.out.println(diffV);
		double dist = diffV.getMagnitude().eval()
				/ (1 + Math.sqrt(c2.getCharge() / c1.getCharge()));
		System.out.println(dist);
		diffV.makeUnitVector();
		diffV.multiply(dist);
		return new FieldCalculator(c1.getXPos() + diffV.getI().eval(),
				c1.getYPos() + diffV.getJ().eval());

	}

}
