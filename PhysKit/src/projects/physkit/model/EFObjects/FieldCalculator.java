package projects.physkit.model.EFObjects;

import java.util.List;

import projects.physkit.model.AbstractFieldObject;
import projects.physkit.model.FieldObject;
import projects.physkit.tools.CoulombsToolkit;
import projects.physkit.tools.PhysVector;

public class FieldCalculator extends AbstractFieldObject {

	private static int defaultNameCount = 0;

	private PhysVector fieldVector = new PhysVector();

	public FieldCalculator(String name, double xPos, double yPos) {
		super(name, xPos, yPos);
	}

	public FieldCalculator(double xPos, double yPos) {
		super("FieldCalc " + defaultNameCount++, xPos, yPos);
	}

	public PhysVector getFieldVector() {
		return fieldVector;
	}

	public double getFieldMagnitude() {
		return fieldVector.getMagnitude().eval(xPos, yPos);
	}

	@Override
	public void update(List<FieldObject> efobjects) {
		fieldVector = new PhysVector();
		for (FieldObject ob : efobjects)
			if (ob instanceof Charge)
				fieldVector.add(CoulombsToolkit.getFieldVector(xPos, yPos,
						(Charge) ob));
			else if (ob instanceof UniformField)
				fieldVector.add(((UniformField) ob).getField());
	}

	@Override
	public Object[] getValues() {
		return new Object[] { "Name=", name, true, "X=" + xPos, true,
				"Y=" + yPos, true, "Field Vector=", fieldVector, false,
				"Field Magnitude=", fieldVector.getMagnitude(), false };
	}
}
