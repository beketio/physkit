package projects.physkit.model.EFObjects;

import java.util.List;

import projects.physkit.model.AbstractFieldObject;
import projects.physkit.model.FieldObject;
import projects.physkit.tools.ExpressionWrapper;
import projects.physkit.tools.PhysVector;

public class UniformField extends AbstractFieldObject {

	private PhysVector vec;

	public UniformField(PhysVector vec) {
		super("", 0, 0);
		this.vec = vec;
	}

	public UniformField(double xField, double yField) {
		super("", 0, 0);
		vec = new PhysVector(ExpressionWrapper.make(xField), ExpressionWrapper.make(yField),
				ExpressionWrapper.make());
	}

	public UniformField(String xField, String yField) {
		super("", 0, 0);
		vec = new PhysVector(ExpressionWrapper.make(xField), ExpressionWrapper.make(yField),
				ExpressionWrapper.make());
	}

	public PhysVector getField() {
		return vec;
	}

	@Override
	public Object[] getValues() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(List<FieldObject> efobjects) {
	}

}
