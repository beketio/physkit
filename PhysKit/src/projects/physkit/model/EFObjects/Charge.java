package projects.physkit.model.EFObjects;

import java.util.ArrayList;
import java.util.List;

import projects.physkit.model.AbstractFieldObject;
import projects.physkit.model.FieldObject;
import projects.physkit.tools.CoulombsToolkit;
import projects.physkit.tools.PhysVector;

public class Charge extends AbstractFieldObject {

	private double charge;
	private List<PhysVector> forceVectors = new ArrayList<PhysVector>();
	private PhysVector forceVector = new PhysVector();

	public Charge(String name, double xPos, double yPos, double charge) {
		super(name, xPos, yPos);
		this.charge = charge;
	}

	public double getCharge() {
		return charge;
	}

	public PhysVector[] getForceVectors() {
		return forceVectors.toArray(new PhysVector[forceVectors.size()]);
	}

	public PhysVector getForceVector() {
		return forceVector;
	}

	public double getForceMagnitude() {
		return forceVector.getMagnitude().eval(xPos, yPos);
	}

	public void update(List<FieldObject> efobjects) {
		for (FieldObject ob : efobjects)
			if (ob instanceof Charge && ob != this)
				forceVectors.add(CoulombsToolkit.getForceVector(this,
						(Charge) ob));
		forceVector = new PhysVector();
		for (PhysVector v : forceVectors)
			forceVector.add(v);
	}

	@Override
	public Object[] getValues() {
		return new Object[] { "Name=", name, true, "X=" + xPos, true,
				"Y=" + yPos, true, "Charge=" + charge, true, "Force Vector=",
				forceVector, false, "Force Magnitude=",
				forceVector.getMagnitude(), false };
	}

}
