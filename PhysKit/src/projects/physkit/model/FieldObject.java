package projects.physkit.model;

import java.util.List;

public interface FieldObject {

	// Getters

	String getName();

	double getXPos();

	double getYPos();

	Object[] getValues();

	// Setters

	void setName(String name);

	void setXPos(double xPos);

	void setYPos(double yPos);

	void setPos(double xPos, double yPos);

	void update(List<FieldObject> efobjects);

}
