package projects.physkit.model;

public abstract class AbstractFieldObject implements FieldObject {

	protected String name;
	protected double xPos, yPos;

	protected AbstractFieldObject(String name, double xPos, double yPos) {
		this.name = name;
		this.xPos = xPos;
		this.yPos = yPos;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getXPos() {
		return xPos;
	}

	@Override
	public double getYPos() {
		return yPos;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void setXPos(double xPos) {
		this.xPos = xPos;
	}

	@Override
	public void setYPos(double yPos) {
		this.yPos = yPos;
	}

	@Override
	public void setPos(double xPos, double yPos) {
		this.xPos = xPos;
		this.yPos = yPos;
	}

	@Override
	public String toString() {
		return "type= " + this.getClass().getName() + " name=" + name
				+ " xPos=" + xPos + " yPos=" + yPos;
	}

}
