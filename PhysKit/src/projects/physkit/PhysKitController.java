package projects.physkit;

import projects.physkit.globalvars.ObRefs;
import projects.physkit.model.EField;

public class PhysKitController {

	public PhysKitController() {
		initModel();
		initGUI();
		new Ogltest().run();
	}

	private void initModel() {
		ObRefs.eField = new EField();
	}

	private void initGUI() {
		ObRefs.guiFrame = new PKFrame();
	}

	public static void main(String[] args) {
		new PhysKitController();
	}

}
