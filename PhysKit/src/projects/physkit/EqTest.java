package projects.physkit;

import net.objecthunter.exp4j.*;
import net.objecthunter.exp4j.function.Function;
import net.objecthunter.exp4j.function.Functions;
import org.mbertoli.jfep.*;

public class EqTest {

	public static void main(String[] args) {
		new EqTest();
	}

	public EqTest() {
		ExpressionBuilder eb = new ExpressionBuilder("5x+6");
		org.nfunk.jep.JEP jepp = new org.nfunk.jep.JEP();
		jepp.parseExpression("5+3*x");
		jepp.setAllowUndeclared(true);
		Parser p = new Parser("5+6*x+3+7*x");
		System.out.println(jepp);
		System.out.println(p.getExpression());
	}
}
