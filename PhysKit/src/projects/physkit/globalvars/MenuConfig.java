package projects.physkit.globalvars;

public class MenuConfig {

	public static final Object[] config = { "Show Charge name",
			GraphConfig.paintChargeName, "Show Charge Value",
			GraphConfig.paintChargeValue, "Show Charge Position",
			GraphConfig.paintChargePos, "Show Charge Magnitude",
			GraphConfig.paintChargeMagnitude, "Show Charge Vector",
			GraphConfig.paintChargeVector, "Show FieldCalculor Name",
			GraphConfig.paintFCName, "Show FieldCalculator Position",
			GraphConfig.paintFCPos, "Show FieldCalculator Magnitude",
			GraphConfig.paintFCMagnitude, "Show FieldCalculator Vector",
			GraphConfig.paintFCVector };

}
