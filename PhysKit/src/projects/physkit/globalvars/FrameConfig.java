package projects.physkit.globalvars;

public class FrameConfig {

	public static final int DEFAULT_FRAME_WIDTH = 800;
	public static final int DEFAULT_FRAME_HEIGHT = 800;

	private FrameConfig() {

	}
}
