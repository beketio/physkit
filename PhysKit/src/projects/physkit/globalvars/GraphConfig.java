package projects.physkit.globalvars;

import java.util.HashMap;
import java.util.Map;

public class GraphConfig {

	public static final int DEAULT_POS_X = 0;
	public static final int DEAULT_POS_Y = 0;
	public static final double DEAULT_SCALE_X = 1;
	public static final double DEAULT_SCALE_Y = 1;
	public static final double DEAULT_STEP_X = 50;
	public static final double DEAULT_STEP_Y = 50;

	public static double xPos = DEAULT_POS_X, yPos = DEAULT_POS_Y;
	public static double xScale = DEAULT_SCALE_X, yScale = DEAULT_SCALE_Y;
	public static double xStep = DEAULT_STEP_X, yStep = DEAULT_STEP_Y;

	public static boolean paintChargeName = false;
	public static boolean paintChargeValue = true;
	public static boolean paintChargePos = true;
	public static boolean paintChargeMagnitude = true;
	public static boolean paintChargeVector = true;
	public static boolean paintChargeVectors = true;
	public static boolean paintFCName = false;
	public static boolean paintFCPos = true;
	public static boolean paintFCMagnitude = true;
	public static boolean paintFCVector = true;

	public static Map<String, Boolean> viewMap = new HashMap<String, Boolean>();

	static {
		viewMap.put("Paint Charge Name", false);
		viewMap.put("Show Charge name", false);
		viewMap.put("Show Charge Value", false);
		viewMap.put("Show Charge Position", false);
		viewMap.put("Show Charge Magnitude", false);
		viewMap.put("Show Charge Vector", false);
		viewMap.put("Show FieldCalculor Name", false);
		viewMap.put("Show FieldCalculator Position", false);
		viewMap.put("Show FieldCalculator Magnitude", false);
		viewMap.put("Show FieldCalculator Vector", false);
	}

}
